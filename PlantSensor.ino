#include <Arduino.h>
//to edit in VS Code: open folder with vscode, hit Ctrl+Shift+P, initialize, then select board
int ANALOG_PIN = 15;
int DIGITAL_PIN = 3;


void setup() {
  pinMode(ANALOG_PIN, INPUT);
  pinMode(DIGITAL_PIN, INPUT);

  Serial.begin(9600);

}

void loop() {
  //read value of moisture levels
  int waterLevel = analogRead(ANALOG_PIN);
  Serial.print("analog level ");
  Serial.println(waterLevel);
  delay(1000);
}
